angular.module('shop.controllers')
	.controller('PaymentCtrl', function($scope, RequestService, CurrentUser, $stateParams, API_HOST, $state){

		$scope.paymentForm = function(){
			RequestService.getDetails(API_HOST, {}, '/orders/' + $stateParams.id).then(function (result) {
                console.log('Order Details...', JSON.stringify(result))
                $scope.order = result.data.order
                if ($scope.order.state != "cart") {
            		$state.go('tab.cart');
                }

            },function(error){
            	console.log("error")
                // utilsService.handleException(error)

            })		
		}

		$scope.toggleObject = function(object) {
		    if ($scope.isObjectShown(object)) {
		      $scope.shownObject = null;
		    } else {
		      $scope.shownObject = object;
		    }
		};
		
		$scope.isObjectShown = function(object) {
		    return $scope.shownObject === object;
		};


		$scope.card = {}
		$scope.doPayment = function(amount){
			console.log('Payment Details...', JSON.stringify($scope.card))
			RequestService.postDetails(API_HOST, {card: $scope.card, amount: amount}, '/payments').then(function (result) {
                console.log('Order Details...', JSON.stringify(result))
                // $state.go('tab.orders')
                $state.go('tab.orders');
            },function(error){
            	console.log("error")
                // utilsService.handleException(error)

            })	
		}

	})