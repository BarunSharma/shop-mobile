angular.module("shop.controllers")
	.controller('ProductCtrl', function($scope, utilsService, CurrentUser, RequestService, API_HOST, $state){

		$scope.item = CurrentUser.getObject('item')
		$scope.getItemDetails = function(){
			$scope.utilsservice = utilsService
			$scope.quantity = $scope.item.amount
			console.log('Item To Edit id:' + JSON.stringify(CurrentUser.getObject('item')))
		}

		$scope.updateItem = function(item){
			console.log('Updated item..' + JSON.stringify(item))
			RequestService.putDetails(API_HOST, {amount: item.amount}, '/carts/' + item.id).then(function (result) {
                console.log('Update Item result...', JSON.stringify(result))
                $scope.item.amount = result.data.item.amount
                $scope.quantity = result.data.item.amount
                $scope.item.price = result.data.item.price
                $state.go('tab.cart', {}, {reload: true});
            },function(error){
            	console.log("error")
                // utilsService.handleException(error)

            })
		}

	})