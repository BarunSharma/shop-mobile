angular.module('shop.controllers')
	.controller('OrderCtrl', function($scope, RequestService, API_HOST, $ionicHistory, $stateParams, $ionicHistory){

		$scope.history = $ionicHistory

        $scope.add2Cart = function(product){
			console.log('product' + JSON.stringify(product.amount));
			console.log('item...' + JSON.stringify($scope.item));
			RequestService.postDetails(API_HOST, {item: {variant_id: $scope.item.id, amount: product.amount}}, '/orders').then(function (result) {
                console.log('Order Details...', JSON.stringify(result))
                CurrentUser.set('cart_items', result.data.total_items);
                $ionicHistory.goBack();
            },function(error){
            	console.log("error")
                // utilsService.handleException(error)

            })
		}

		$scope.orders = {}	
		$scope.allOrders = function(){
			RequestService.getDetails(API_HOST, {}, '/orders').then(function (result) {
                console.log('Order Details...', JSON.stringify(result))
                $scope.orders.placed = result.data.placed
                $scope.orders.picked = result.data.picked
                $scope.orders.delivered = result.data.delivered
            },function(error){
            	console.log("error")
                // utilsService.handleException(error)

            })
		}

        $scope.orderDetails = function() {
            RequestService.getDetails(API_HOST, {}, '/orders/' + $stateParams.id).then(function (result) {
                console.log('Order Details...', JSON.stringify(result))
                $scope.order = result.data.order
                $scope.items = result.data.items
                $scope.items_count = result.data.items_count
                $scope.total_price = result.data.total_price
            },function(error){
                console.log("error")
                // utilsService.handleException(error)

            })
        }

        $scope.toggleObject = function(object) {
            if ($scope.isObjectShown(object)) {
              $scope.shownObject = null;
            } else {
              $scope.shownObject = object;
            }
        };
        
        $scope.isObjectShown = function(object) {
            return $scope.shownObject === object;
        };

	})