angular.module('shop.controllers')
	.controller('CartCtrl', function($scope, RequestService, API_HOST, utilsService){

		$scope.utilsservice = utilsService
		$scope.all_items = {}
		$scope.cartItems = function(){
			RequestService.getDetails(API_HOST, {}, '/carts').then(function (result) {
                console.log('Cart Items...', JSON.stringify(result))
                $scope.all_items = result.data.items
                $scope.items_count = result.data.items_count
                $scope.total_price = result.data.total_price
            },function(error){
            	console.log("error")
                // utilsService.handleException(error)
            })
		};

		$scope.data = {
		    showDelete: false
	    };

	    $scope.itemDelete = function(item){
	    	console.log("Item" + JSON.stringify(item))
	    	RequestService.postDetails(API_HOST, {id: item.id}, '/carts/remove_item').then(function (result) {
                console.log('Cart Items...', JSON.stringify(result))
                $scope.items_count = result.data.items_count
                $scope.total_price = result.data.total_price
                $scope.all_items.splice($scope.all_items.indexOf(item), 1);
            },function(error){
            	console.log("error")
                // utilsService.handleException(error)
            })
	    	
	    }

	    $scope.checkOut = function(){
	    	
	    }

	})