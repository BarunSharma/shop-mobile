angular.module('shop.controllers')
	.controller('PublicCtrl', function($scope, $state, RequestService, $filter, CurrentUser, API_HOST, $ionicSlideBoxDelegate, utilsService){

		$scope.homePage = function(){

            $scope.nextSlide = function() {
                $ionicSlideBoxDelegate.next();
            }

            $scope.previousSlide = function() {
                $ionicSlideBoxDelegate.previous();
            }

		}

		$scope.user = {}
		$scope.signUp = function(){
			$scope.user.country_code = "91";
		}

		
		$scope.doRegister =function(){
			var mobile_number = $filter('phoneInput')($scope.user.country_code, $scope.user.mobile_number)
			console.log('Sign up form details' + JSON.stringify($scope.user) + mobile_number)
			RequestService.postDetails(API_HOST, {user: { mobile_number: mobile_number, country_code: $scope.user.country_code, name: $scope.user.name}}, '/users').then(function (result) {
                console.log('User Result' + JSON.stringify(result))
                $scope.mobile = {}
                $scope.mobile.number = result.data.mobile_number
                CurrentUser.setObject('mobile', $scope.mobile)
                console.log('New user record...' + JSON.stringify(CurrentUser.getObject('mobile')));
                $state.go('public.activate')
            },function(error){
            	console.log(error)
                utilsService.handleException(error)
            })
		}

		$scope.activationForm = function() {

		}

		$scope.doActivate = function(){
            var mobile_number = CurrentUser.getObject('mobile').number;
            console.log('Details with activation code...' + JSON.stringify($scope.user));
            console.log('Current user mobile...' + JSON.stringify(CurrentUser.getObject('mobile')));
            RequestService.postDetails(API_HOST, {user: { mobile_number: mobile_number, otp: $scope.user.otp}}, '/users/activate').then(function (result) {
                console.log('User Result' + JSON.stringify(result));
                console.log('User Result Data' + JSON.stringify(result.data.access_token));
                CurrentUser.set('access_token' , result.data.access_token);
                console.log('access_token' + CurrentUser.get('access_token'));
                $state.go('tab.dash')
            },function(error){
            	console.log(error)
                utilsService.handleException(error)
            })

        }
		
	})