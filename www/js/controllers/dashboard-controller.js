angular.module('shop.controllers')
	.controller('DashCtrl', function($scope, $state, RequestService, API_HOST, $stateParams, utilsService, $ionicHistory){

        $scope.history = $ionicHistory

		$scope.categories = {}
		$scope.selectCategory = function(){
			RequestService.getDetails(API_HOST, {}, '/categories').then(function (result) {
                console.log('Categories...', JSON.stringify(result))
                $scope.categories = result.data.categories
                $scope.chunkedCategories = [];
                while ($scope.categories.length > 0)
 					$scope.chunkedCategories.push($scope.categories.splice(0, 2));
                utilsService.handleException("message")
            },function(error){
            	console.log("error")
                utilsService.handleException("error")

            })
		}


		$scope.category = {}
		$scope.children = {}
		$scope.allChilds = function(){
			RequestService.getDetails(API_HOST, {}, '/categories/' + $stateParams.id).then(function (result) {
                console.log('Category details...', JSON.stringify(result))
                $scope.category = result.data.category
                $scope.children = result.data.children
                $scope.chunkedChildCategories = [];
                while ($scope.children.length > 0)
                    $scope.chunkedChildCategories.push($scope.children.splice(0, 2));

            },function(error){
            	console.log("error")
                // utilsService.handleException(error)

            })	
		}

		$scope.child = {}
		$scope.loadChild = function(){
			RequestService.getDetails(API_HOST, {}, '/child/' + $stateParams.id).then(function (result) {
                console.log('Child category details...', JSON.stringify(result))
                $scope.child = result.data.child
                $scope.products = result.data.products
                $scope.chunkedProducts = [];
                while ($scope.products.length > 0)
                    $scope.chunkedProducts.push($scope.products.splice(0, 2));

            },function(error){
            	console.log("error")
                // utilsService.handleException(error)

            })	
		}

		$scope.product = {}
		$scope.options_text = 'Variant';
		$scope.item =  {variant: 'Variant', price: 'Select', id: null};
		$scope.loadProduct = function(){
			RequestService.getDetails(API_HOST, {}, '/products/' + $stateParams.id).then(function (result) {
                console.log('Product details...', JSON.stringify(result))
                $scope.product = result.data.product
                $scope.options = result.data.prices;
                if ($scope.product.weight) {
                	$scope.item = {variant: $scope.options[0].variant, price: $scope.options[0].price, id: $scope.options[0].id}
                }

            },function(error){
            	console.log("error")
                // utilsService.handleException(error)

            })	
		}

		$scope.toggleObject = function(object) {
		    if ($scope.isObjectShown(object)) {
		      $scope.shownObject = null;
		    } else {
		      $scope.shownObject = object;
		    }
		};
		
		$scope.isObjectShown = function(object) {
		    return $scope.shownObject === object;
		};

	})