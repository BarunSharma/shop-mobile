// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('shop', ['ionic', , 'ngCordova', 'shop.controllers', 'shop.services', 'shop.directives','shop.filters', 'shop.constants', 'angularPayments'])

.run(function($ionicPlatform, $rootScope, LoaderService) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)

    $rootScope.$on('loading:show', function() {
       LoaderService.show()
    })

    $rootScope.$on('loading:hide', function() {
       LoaderService.hide()
    })

    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleLightContent();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider, $httpProvider, $ionicConfigProvider) {

  $httpProvider.defaults.useXDomain = true;
  $httpProvider.defaults.withCredentials = true;
  delete $httpProvider.defaults.headers.common["X-Requested-With"];
  $httpProvider.defaults.headers.common["Accept"] = "application/json";
  $httpProvider.defaults.headers.common["Content-Type"] = "application/json";
  $httpProvider.interceptors.push('accessTokenInterceptor');
  $httpProvider.interceptors.push('timeoutHttpIntercept');
  $ionicConfigProvider.tabs.position('bottom');
  $ionicConfigProvider.views.swipeBackEnabled(false);

  $httpProvider.interceptors.push(function ($rootScope) {
      return {
          request: function (config) {
              $rootScope.$broadcast('loading:show')
              return config
          },
          response: function (response) {
              $rootScope.$broadcast('loading:hide')
              return response
          }
      }
  })

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the public directive
  .state('public', {
    url: "/public",
    abstract: true,
    templateUrl: "templates/layout/public.html"
  })

  .state('public.home', {
    url: '/home',
    views: {
      'publicContent': {
        templateUrl: 'templates/public/home.html',
        controller: 'PublicCtrl'
      }
    }
  })

  .state('public.signup', {
    url: '/signup',
    views: {
      'publicContent': {
        templateUrl: 'templates/public/signup.html',
        controller: 'PublicCtrl'
      }
    }
  })

  .state('public.activate', {
    url: '/activate',
    views: {
      'publicContent': {
        templateUrl: 'templates/public/activate.html',
        controller: 'PublicCtrl'
      }
    }
  })

  // setup an abstract state for the tabs directive
  .state('tab', {
    url: "/tab",
    abstract: true,
    templateUrl: "templates/layout/member.html"
  })

  // Each tab has its own nav history stack:

  .state('tab.dash', {
    url: '/dash',
    views: {
      'tab-dash': {
        templateUrl: 'templates/member/dashboard.html',
        controller: 'DashCtrl'
      }
    }
  })

  .state('tab.childs', {
    url: '/dash/:id',
    views: {
      'tab-dash': {
        templateUrl: 'templates/member/childs.html',
        controller: 'DashCtrl'
      }
    }
  })

  .state('tab.child', {
    url: '/dash/child/:id',
    views: {
      'tab-dash': {
        templateUrl: 'templates/member/child.html',
        controller: 'DashCtrl'
      }
    }
  })

  .state('tab.product', {
    url: '/dash/product/:id',
    views: {
      'tab-dash': {
        templateUrl: 'templates/member/product.html',
        controller: 'DashCtrl'
      }
    }
  })

  .state('tab.cart', {
    url: '/cart',
    views: {
      'tab-cart': {
        templateUrl: 'templates/member/cart/index.html',
        controller: 'CartCtrl'
      }
    }
  })

  .state('tab.payment', {
    url: '/payment/:id',
    views: {
      'tab-cart': {
        templateUrl: 'templates/member/payment/index.html',
        controller: 'PaymentCtrl'
      }
    }
  })

  .state('tab.orders', {
    url: '/orders',
    views: {
      'tab-orders': {
        templateUrl: 'templates/member/orders/index.html',
        controller: 'OrderCtrl'
      }
    }
  })

  .state('tab.order', {
    url: '/orders/:id',
    views: {
      'tab-orders': {
        templateUrl: 'templates/member/orders/show.html',
        controller: 'OrderCtrl'
      }
    }
  })


  .state('tab.account', {
    url: '/account',
    views: {
      'tab-account': {
        templateUrl: 'templates/member/account/index.html',
        controller: 'AccountCtrl'
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  if(window.localStorage['access_token'] && window.localStorage['access_token'] !== null) {
      console.log('mobile', JSON.parse(window.localStorage['mobile']));
      $urlRouterProvider.otherwise('/tab/dash');
  }
  else {
      $urlRouterProvider.otherwise('/public/home');
  }

});
