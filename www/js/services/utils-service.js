angular.module('shop.services')
    .service('utilsService', function($ionicModal, LoaderService, AlertService, $state, CurrentUser, $cordovaNetwork, $ionicHistory) {
    this.handleException = function(error, message, event) {
        console.log('error', JSON.stringify(error))
        LoaderService.hide()
        if(window.Connection) {
            if($cordovaNetwork.isOffline()) {
                AlertService.showToast(APP_CONFIG.OFFLINE_MSG, 'long', 'center')
                return
            }
        }
        if(error.status == 0) {
            AlertService.showToast(APP_CONFIG.SERVER_CONNECT_ERROR, 'long', 'center')
            return
        }
        if(error.status == 500) {
            AlertService.showToast(message || APP_CONFIG.SERVER_ERROR, 'long', 'center')
            return
        }
        if(error.status == 422) {
            if (error.data.error) {
                AlertService.showToast(error.data.error, 'short', 'center')
                return
            }
        }
        if (error.status == 401) {
            CurrentUser.delete('access_token')
            if(message != undefined ){
                AlertService.showToast(message, 'long', 'center')
            }
            if(event == 'login') {
                $ionicHistory.nextViewOptions({
                    disableAnimate: true,
                    disableBack: true
                });
            }
            $state.go('app.home')
        }
    }

    this.showModal = function (item){
        console.log("item" + JSON.stringify(item))
        CurrentUser.setObject('item', item)
        var service = this;
        $ionicModal.fromTemplateUrl('templates/shared/modal.html', {
            scope: null
        }).then(function(modal) {
                service.productModal = modal;
                service.productModal.show();
            });
    };

    /**
     * Hide modal for avatar upload
     */
    this.hideModal = function (){
        if(this.productModal) {
            this.productModal.hide();
        }
    }


})