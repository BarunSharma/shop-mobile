angular.module('shop.services')
    .factory('timeoutHttpIntercept', function ($rootScope, $q) {
    return {
        'request': function (config) {
            config.timeout = 60000;
            return config;
        }
    };
})