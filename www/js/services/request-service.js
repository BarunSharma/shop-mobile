angular.module('shop.services')
.factory('RequestService', function($http) {
    return {
        getDetails: function (host, params, uri) {
            return $http({ method: 'get', url: host+uri, params: params, headers: {'Content-Type': 'application/json'}})
        },
        postDetails: function (host, data, uri) {
            return $http({method: 'POST', url: host+uri,  headers: {'Content-Type': 'application/json'}, data: data})
        },
        putDetails: function (host, data, uri) {
            return $http({method: 'PUT', url: host+uri,  headers: {'Content-Type': 'application/json'}, data: data})
        },
        deleteDetails: function (host, params, uri) {
            return $http({method: 'DELETE', url: host+uri, params: params, headers: {'Content-Type': 'application/json'}})
        }
    }
})
