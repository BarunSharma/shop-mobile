angular.module('shop.services')
    .factory('AlertService', function($ionicPopup, $cordovaToast){
    return {
        showToast: function(msg, size, location){
            if (window.plugins && window.plugins.toast) {
                $cordovaToast.show(msg, size, location).then(function() {
                    console.log('toast shown')
                }, function (error) {
                    console.log("The toast was not shown due to " + error);
                });
            } else {
                $ionicPopup.alert({
                    content: msg
                }).then(function(){

                })
            }
        }
    }
})