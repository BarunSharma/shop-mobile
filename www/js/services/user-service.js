angular.module('shop.services')
.factory('CurrentUser', function($window) {

    return {
        set: function (key, value) {
            $window.localStorage[key] = value;
        },
        get: function (key) {
            return $window.localStorage[key] || null;
        },
        delete: function(key) {
            delete  $window.localStorage[key]
        },
        setObject: function (key, value) {
            $window.localStorage[key] = JSON.stringify(value);
        },
        setArray: function (key, value) {
            $window.localStorage[key] = JSON.stringify(value);
        },
        getObject: function (key) {
            return JSON.parse($window.localStorage[key] || '{}');
        },
        getArray: function (key) {
            return JSON.parse($window.localStorage[key] || '[]');
        },
        deleteAll: function() {
            $window.localStorage.clear();
        }
    }
})