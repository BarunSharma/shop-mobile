angular.module('shop.services')
    .factory('accessTokenInterceptor', function (CurrentUser) {
        return {
            request: function (config) {
                var token = CurrentUser.get("access_token");
                if (config.data === undefined) {
                    //Do nothing if data is not originally supplied from the calling method
                    config.data = {}
                    config.data.access_token = token ;
                }
                else {
                    config.data.access_token = token;
                }

                if (config.method === 'GET') {
                    if (config.params === undefined) {
                        config.params = {};
                    }
                    config.params.access_token = token;
                }
                return config;
            }
        };
    });