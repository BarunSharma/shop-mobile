angular.module('shop.filters',[])
angular.module('shop.filters')
    .filter('tel', function () {
        return function (tel) {
            if (!tel) { return null; }

            var value = tel.toString().trim().replace(/\D/g, '');
//            console.log(value)
            if (value.match(/[^0-9]/)) {
                return tel;
            }

            var country, number;

            if (value.length < 10) {
                return null;
            }
            else if(value.length == 10) {
                country = 91;
                number = value;
            }
            else {
                country = value.slice(0, value.length - 10)
                if (country == 0) {
                    country = 91
                }
                number = value.slice(value.length - 10);
            }
            return (country + number).trim();
        };
    })

    .filter('phoneInput', function () {
        return function (countryCode, tel) {
            if (!tel) { return null; }

            var value = tel.toString().trim().replace(/\D/g, '');
            if (value.match(/[^0-9]/)) {
                return tel;
            }
            return (countryCode + value).trim();
        }
    });