angular.module('shop.services',[])
angular.module('shop.services')
    .factory('AlertService', function($ionicPopup, $cordovaToast){
    return {
        showToast: function(msg, size, location){
            if (window.plugins && window.plugins.toast) {
                $cordovaToast.show(msg, size, location).then(function() {
                    console.log('toast shown')
                }, function (error) {
                    console.log("The toast was not shown due to " + error);
                });
            } else {
                $ionicPopup.alert({
                    content: msg
                }).then(function(){

                })
            }
        }
    }
})
angular.module('shop.services')
    .factory('LoaderService', function($ionicLoading) {

    // Trigger the loading indicator
    return {
        show : function() { //code from the ionic framework doc

            // Show the loading overlay and text
            $ionicLoading.show({

                // The text to display in the loading indicator
                content: 'Loading',

                // The animation to use
                animation: 'fade-in',

                // Will a dark overlay or backdrop cover the entire view
                showBackdrop: true,

                // The maximum width of the loading indicator
                // Text will be wrapped if longer than maxWidth
                maxWidth: 200,

                // The delay in showing the indicator
                showDelay: 500
            });
        },
        hide : function(){
            $ionicLoading.hide();
        }
    }
})
angular.module('shop.services')
    .factory('accessTokenInterceptor', function (CurrentUser) {
        return {
            request: function (config) {
                var token = CurrentUser.get("access_token");
                if (config.data === undefined) {
                    //Do nothing if data is not originally supplied from the calling method
                    config.data = {}
                    config.data.access_token = token ;
                }
                else {
                    config.data.access_token = token;
                }

                if (config.method === 'GET') {
                    if (config.params === undefined) {
                        config.params = {};
                    }
                    config.params.access_token = token;
                }
                return config;
            }
        };
    });
angular.module('shop.services')
.factory('RequestService', function($http) {
    return {
        getDetails: function (host, params, uri) {
            return $http({ method: 'get', url: host+uri, params: params, headers: {'Content-Type': 'application/json'}})
        },
        postDetails: function (host, data, uri) {
            return $http({method: 'POST', url: host+uri,  headers: {'Content-Type': 'application/json'}, data: data})
        },
        putDetails: function (host, data, uri) {
            return $http({method: 'PUT', url: host+uri,  headers: {'Content-Type': 'application/json'}, data: data})
        },
        deleteDetails: function (host, params, uri) {
            return $http({method: 'DELETE', url: host+uri, params: params, headers: {'Content-Type': 'application/json'}})
        }
    }
})

angular.module('shop.services')
    .factory('timeoutHttpIntercept', function ($rootScope, $q) {
    return {
        'request': function (config) {
            config.timeout = 60000;
            return config;
        }
    };
})
angular.module('shop.services')
.factory('CurrentUser', function($window) {

    return {
        set: function (key, value) {
            $window.localStorage[key] = value;
        },
        get: function (key) {
            return $window.localStorage[key] || null;
        },
        delete: function(key) {
            delete  $window.localStorage[key]
        },
        setObject: function (key, value) {
            $window.localStorage[key] = JSON.stringify(value);
        },
        setArray: function (key, value) {
            $window.localStorage[key] = JSON.stringify(value);
        },
        getObject: function (key) {
            return JSON.parse($window.localStorage[key] || '{}');
        },
        getArray: function (key) {
            return JSON.parse($window.localStorage[key] || '[]');
        },
        deleteAll: function() {
            $window.localStorage.clear();
        }
    }
})
angular.module('shop.services')
    .service('utilsService', function($ionicModal, LoaderService, AlertService, $state, CurrentUser, $cordovaNetwork, $ionicHistory) {
    this.handleException = function(error, message, event) {
        console.log('error', JSON.stringify(error))
        LoaderService.hide()
        if(window.Connection) {
            if($cordovaNetwork.isOffline()) {
                AlertService.showToast(APP_CONFIG.OFFLINE_MSG, 'long', 'center')
                return
            }
        }
        if(error.status == 0) {
            AlertService.showToast(APP_CONFIG.SERVER_CONNECT_ERROR, 'long', 'center')
            return
        }
        if(error.status == 500) {
            AlertService.showToast(message || APP_CONFIG.SERVER_ERROR, 'long', 'center')
            return
        }
        if(error.status == 422) {
            if (error.data.error) {
                AlertService.showToast(error.data.error, 'short', 'center')
                return
            }
        }
        if (error.status == 401) {
            CurrentUser.delete('access_token')
            if(message != undefined ){
                AlertService.showToast(message, 'long', 'center')
            }
            if(event == 'login') {
                $ionicHistory.nextViewOptions({
                    disableAnimate: true,
                    disableBack: true
                });
            }
            $state.go('app.home')
        }
    }

    this.showModal = function (item){
        console.log("item" + JSON.stringify(item))
        CurrentUser.setObject('item', item)
        var service = this;
        $ionicModal.fromTemplateUrl('templates/shared/modal.html', {
            scope: null
        }).then(function(modal) {
                service.productModal = modal;
                service.productModal.show();
            });
    };

    /**
     * Hide modal for avatar upload
     */
    this.hideModal = function (){
        if(this.productModal) {
            this.productModal.hide();
        }
    }


})