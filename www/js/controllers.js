angular.module("shop.controllers",[])
angular.module('shop.controllers')
	.controller('AccountCtrl', function($scope, RequestService, API_HOST, utilsService){

	    $scope.loadAccount = function(){
	    	
	    }

	})
angular.module('shop.controllers')
	.controller('CartCtrl', function($scope, RequestService, API_HOST, utilsService){

		$scope.utilsservice = utilsService
		$scope.all_items = {}
		$scope.cartItems = function(){
			RequestService.getDetails(API_HOST, {}, '/carts').then(function (result) {
                console.log('Cart Items...', JSON.stringify(result))
                $scope.all_items = result.data.items
                $scope.items_count = result.data.items_count
                $scope.total_price = result.data.total_price
            },function(error){
            	console.log("error")
                // utilsService.handleException(error)
            })
		};

		$scope.data = {
		    showDelete: false
	    };

	    $scope.itemDelete = function(item){
	    	console.log("Item" + JSON.stringify(item))
	    	RequestService.postDetails(API_HOST, {id: item.id}, '/carts/remove_item').then(function (result) {
                console.log('Cart Items...', JSON.stringify(result))
                $scope.items_count = result.data.items_count
                $scope.total_price = result.data.total_price
                $scope.all_items.splice($scope.all_items.indexOf(item), 1);
            },function(error){
            	console.log("error")
                // utilsService.handleException(error)
            })
	    	
	    }

	    $scope.checkOut = function(){
	    	
	    }

	})
angular.module('shop.controllers')
	.controller('DashCtrl', function($scope, $state, RequestService, API_HOST, $stateParams, utilsService, $ionicHistory){

        $scope.history = $ionicHistory

		$scope.categories = {}
		$scope.selectCategory = function(){
			RequestService.getDetails(API_HOST, {}, '/categories').then(function (result) {
                console.log('Categories...', JSON.stringify(result))
                $scope.categories = result.data.categories
                $scope.chunkedCategories = [];
                while ($scope.categories.length > 0)
 					$scope.chunkedCategories.push($scope.categories.splice(0, 2));
                utilsService.handleException("message")
            },function(error){
            	console.log("error")
                utilsService.handleException("error")

            })
		}


		$scope.category = {}
		$scope.children = {}
		$scope.allChilds = function(){
			RequestService.getDetails(API_HOST, {}, '/categories/' + $stateParams.id).then(function (result) {
                console.log('Category details...', JSON.stringify(result))
                $scope.category = result.data.category
                $scope.children = result.data.children
                $scope.chunkedChildCategories = [];
                while ($scope.children.length > 0)
                    $scope.chunkedChildCategories.push($scope.children.splice(0, 2));

            },function(error){
            	console.log("error")
                // utilsService.handleException(error)

            })	
		}

		$scope.child = {}
		$scope.loadChild = function(){
			RequestService.getDetails(API_HOST, {}, '/child/' + $stateParams.id).then(function (result) {
                console.log('Child category details...', JSON.stringify(result))
                $scope.child = result.data.child
                $scope.products = result.data.products
                $scope.chunkedProducts = [];
                while ($scope.products.length > 0)
                    $scope.chunkedProducts.push($scope.products.splice(0, 2));

            },function(error){
            	console.log("error")
                // utilsService.handleException(error)

            })	
		}

		$scope.product = {}
		$scope.options_text = 'Variant';
		$scope.item =  {variant: 'Variant', price: 'Select', id: null};
		$scope.loadProduct = function(){
			RequestService.getDetails(API_HOST, {}, '/products/' + $stateParams.id).then(function (result) {
                console.log('Product details...', JSON.stringify(result))
                $scope.product = result.data.product
                $scope.options = result.data.prices;
                if ($scope.product.weight) {
                	$scope.item = {variant: $scope.options[0].variant, price: $scope.options[0].price, id: $scope.options[0].id}
                }

            },function(error){
            	console.log("error")
                // utilsService.handleException(error)

            })	
		}

		$scope.toggleObject = function(object) {
		    if ($scope.isObjectShown(object)) {
		      $scope.shownObject = null;
		    } else {
		      $scope.shownObject = object;
		    }
		};
		
		$scope.isObjectShown = function(object) {
		    return $scope.shownObject === object;
		};

	})
angular.module('shop.controllers')
	.controller('OrderCtrl', function($scope, RequestService, API_HOST, $ionicHistory, $stateParams, $ionicHistory){

		$scope.history = $ionicHistory

        $scope.add2Cart = function(product){
			console.log('product' + JSON.stringify(product.amount));
			console.log('item...' + JSON.stringify($scope.item));
			RequestService.postDetails(API_HOST, {item: {variant_id: $scope.item.id, amount: product.amount}}, '/orders').then(function (result) {
                console.log('Order Details...', JSON.stringify(result))
                CurrentUser.set('cart_items', result.data.total_items);
                $ionicHistory.goBack();
            },function(error){
            	console.log("error")
                // utilsService.handleException(error)

            })
		}

		$scope.orders = {}	
		$scope.allOrders = function(){
			RequestService.getDetails(API_HOST, {}, '/orders').then(function (result) {
                console.log('Order Details...', JSON.stringify(result))
                $scope.orders.placed = result.data.placed
                $scope.orders.picked = result.data.picked
                $scope.orders.delivered = result.data.delivered
            },function(error){
            	console.log("error")
                // utilsService.handleException(error)

            })
		}

        $scope.orderDetails = function() {
            RequestService.getDetails(API_HOST, {}, '/orders/' + $stateParams.id).then(function (result) {
                console.log('Order Details...', JSON.stringify(result))
                $scope.order = result.data.order
                $scope.items = result.data.items
                $scope.items_count = result.data.items_count
                $scope.total_price = result.data.total_price
            },function(error){
                console.log("error")
                // utilsService.handleException(error)

            })
        }

        $scope.toggleObject = function(object) {
            if ($scope.isObjectShown(object)) {
              $scope.shownObject = null;
            } else {
              $scope.shownObject = object;
            }
        };
        
        $scope.isObjectShown = function(object) {
            return $scope.shownObject === object;
        };

	})
angular.module('shop.controllers')
	.controller('PaymentCtrl', function($scope, RequestService, CurrentUser, $stateParams, API_HOST, $state){

		$scope.paymentForm = function(){
			RequestService.getDetails(API_HOST, {}, '/orders/' + $stateParams.id).then(function (result) {
                console.log('Order Details...', JSON.stringify(result))
                $scope.order = result.data.order
                if ($scope.order.state != "cart") {
            		$state.go('tab.cart');
                }

            },function(error){
            	console.log("error")
                // utilsService.handleException(error)

            })		
		}

		$scope.toggleObject = function(object) {
		    if ($scope.isObjectShown(object)) {
		      $scope.shownObject = null;
		    } else {
		      $scope.shownObject = object;
		    }
		};
		
		$scope.isObjectShown = function(object) {
		    return $scope.shownObject === object;
		};


		$scope.card = {}
		$scope.doPayment = function(amount){
			console.log('Payment Details...', JSON.stringify($scope.card))
			RequestService.postDetails(API_HOST, {card: $scope.card, amount: amount}, '/payments').then(function (result) {
                console.log('Order Details...', JSON.stringify(result))
                // $state.go('tab.orders')
                $state.go('tab.orders');
            },function(error){
            	console.log("error")
                // utilsService.handleException(error)

            })	
		}

	})
angular.module("shop.controllers")
	.controller('ProductCtrl', function($scope, utilsService, CurrentUser, RequestService, API_HOST, $state){

		$scope.item = CurrentUser.getObject('item')
		$scope.getItemDetails = function(){
			$scope.utilsservice = utilsService
			$scope.quantity = $scope.item.amount
			console.log('Item To Edit id:' + JSON.stringify(CurrentUser.getObject('item')))
		}

		$scope.updateItem = function(item){
			console.log('Updated item..' + JSON.stringify(item))
			RequestService.putDetails(API_HOST, {amount: item.amount}, '/carts/' + item.id).then(function (result) {
                console.log('Update Item result...', JSON.stringify(result))
                $scope.item.amount = result.data.item.amount
                $scope.quantity = result.data.item.amount
                $scope.item.price = result.data.item.price
                $state.go('tab.cart', {}, {reload: true});
            },function(error){
            	console.log("error")
                // utilsService.handleException(error)

            })
		}

	})
angular.module('shop.controllers')
	.controller('PublicCtrl', function($scope, $state, RequestService, $filter, CurrentUser, API_HOST, $ionicSlideBoxDelegate, utilsService){

		$scope.homePage = function(){

            $scope.nextSlide = function() {
                $ionicSlideBoxDelegate.next();
            }

            $scope.previousSlide = function() {
                $ionicSlideBoxDelegate.previous();
            }

		}

		$scope.user = {}
		$scope.signUp = function(){
			$scope.user.country_code = "91";
		}

		
		$scope.doRegister =function(){
			var mobile_number = $filter('phoneInput')($scope.user.country_code, $scope.user.mobile_number)
			console.log('Sign up form details' + JSON.stringify($scope.user) + mobile_number)
			RequestService.postDetails(API_HOST, {user: { mobile_number: mobile_number, country_code: $scope.user.country_code, name: $scope.user.name}}, '/users').then(function (result) {
                console.log('User Result' + JSON.stringify(result))
                $scope.mobile = {}
                $scope.mobile.number = result.data.mobile_number
                CurrentUser.setObject('mobile', $scope.mobile)
                console.log('New user record...' + JSON.stringify(CurrentUser.getObject('mobile')));
                $state.go('public.activate')
            },function(error){
            	console.log(error)
                utilsService.handleException(error)
            })
		}

		$scope.activationForm = function() {

		}

		$scope.doActivate = function(){
            var mobile_number = CurrentUser.getObject('mobile').number;
            console.log('Details with activation code...' + JSON.stringify($scope.user));
            console.log('Current user mobile...' + JSON.stringify(CurrentUser.getObject('mobile')));
            RequestService.postDetails(API_HOST, {user: { mobile_number: mobile_number, otp: $scope.user.otp}}, '/users/activate').then(function (result) {
                console.log('User Result' + JSON.stringify(result));
                console.log('User Result Data' + JSON.stringify(result.data.access_token));
                CurrentUser.set('access_token' , result.data.access_token);
                console.log('access_token' + CurrentUser.get('access_token'));
                $state.go('tab.dash')
            },function(error){
            	console.log(error)
                utilsService.handleException(error)
            })

        }
		
	})