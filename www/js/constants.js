angular.module('shop.constants', [])


.constant('APP_CONFIG', {
    "OFFLINE_MSG": 'You seem to be offline.',
    "SERVER_CONNECT_ERROR": 'Unable to connect to server',
    "SERVER_ERROR": 'Error processing your request, Please try later',
    "PROFILE_UPDATED": 'Profile Updated',
    "COUNTRY_SELECT_ERROR": "Please select country code"
})