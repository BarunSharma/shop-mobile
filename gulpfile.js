var gulp = require('gulp');
var gutil = require('gulp-util');
var bower = require('bower');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var sh = require('shelljs');

var paths = {
    sass: ['./scss/**/*.scss', './www/scss/style.css.scss'],
    controllers: ['./www/js/controllers/*.js'],
    services: ['./www/js/services/*.js'],
    filters: ['./www/js/filters/*.js'],
    directives: ['./www/js/directives/*.js']
};

gulp.task('default', ['sass']);

gulp.task('sass', function(done) {
    gulp.src('./scss/ionic.app.scss')
        .pipe(sass())
        .pipe(gulp.dest('./www/css/'))
        .pipe(minifyCss({
            keepSpecialComments: 0
        }))
        .pipe(rename({ extname: '.min.css' }))
        .pipe(gulp.dest('./www/css/'))
    gulp.src('./www/scss/style.css.scss')
        .pipe(sass())
        .pipe(rename('style.css'))
        .pipe(gulp.dest('./www/css/'))
        .on('end', done);
});

gulp.task('scripts', function(done) {
    gulp.src(paths.controllers)
        .pipe(concat('controllers.js'))
        .pipe(gulp.dest('./www/js'))
    gulp.src(paths.services)
        .pipe(concat('services.js'))
        .pipe(gulp.dest('./www/js'))
    gulp.src(paths.filters)
        .pipe(concat('filters.js'))
        .pipe(gulp.dest('./www/js'))
    gulp.src(paths.directives)
        .pipe(concat('directives.js'))
        .pipe(gulp.dest('./www/js'))    
    gulp.src('./www/config/config-local.js')
        .pipe(concat('config.js'))
        .pipe(gulp.dest('./www/js'))
        .on('end', done);
});

gulp.task('watch', function() {
  gulp.watch(paths.sass, ['sass']);
  gulp.watch(paths.controllers, ['scripts']);
  gulp.watch(paths.services, ['scripts']);
  gulp.watch(paths.filters, ['scripts']);
  gulp.watch(paths.directives, ['scripts']);
});

gulp.task('install', ['git-check'], function() {
  return bower.commands.install()
    .on('log', function(data) {
      gutil.log('bower', gutil.colors.cyan(data.id), data.message);
    });
});

gulp.task('git-check', function(done) {
  if (!sh.which('git')) {
    console.log(
      '  ' + gutil.colors.red('Git is not installed.'),
      '\n  Git, the version control system, is required to download Ionic.',
      '\n  Download git here:', gutil.colors.cyan('http://git-scm.com/downloads') + '.',
      '\n  Once git is installed, run \'' + gutil.colors.cyan('gulp install') + '\' again.'
    );
    process.exit(1);
  }
  done();
});
